// dollarteken --> jquery 
// testen of jquery werkt door boodschap te schrijven naar pagina, die te zien is bij inspecteren > console
// alert --> een pop-up verschijnt
/*
$(function () {
    console.log('Hello from jQuery');
    alert('Hallo daar!');
});
*/



$(document).ready(function() {
  
  $(window).scroll(function () {
    if ($(window).scrollTop() > 655) {
      $('#nav_bar').addClass('navbar-fixed');
    }
    if ($(window).scrollTop() < 656) {
      $('#nav_bar').removeClass('navbar-fixed');
    }
  });
});



$(function () {

        // scroll to top button
        var $scrollToTopButton = $("#scroll-to-top");
        var $body = $("body");
        $scrollToTopButton.on("click", function() {
            //alert("click"); // test: op het moment dat je klikt krijg je een popup
            $body.animate({
                scrollTop: 0
            });
        });
        
        var treshold = window.innerHeight/3;

        $(window).on("scroll", function(){
            if ($body.scrollTop() > treshold ) {
                $scrollToTopButton.show ();
            } else {
                $scrollToTopButton.hide ();
            }
            
            // console.log($body.scrollTop())
        });
    });
