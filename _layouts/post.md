---
layout: master/page
---


<section class="page">

<div class="container">
<div class="breadcrumb">
<a href="{{ 'index.html' | relative_url }}">Home</a> > 
<a href="{{ 'pages/allenieuws.html' | relative_url }}">Alle blogposts</a> >
{{ page.title }}
</div>
</div>

<div class="permalinkpage">

<div class="clearfix nieuwspost">


{{ content }}

</div>
</div>
</section>