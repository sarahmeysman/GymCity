---
layout: post
title: Ook Decathlon steunt ons
categories: post4
---
<h2 class="titel4">Ook Decathlon steunt ons</h2>
<div class="">
<p>Hulp uit onverwachte hoek! Decathlon nam een kijkje naar ons project en vond het de moeite waard om er reclame voor te maken. Dankjewel Decathlon!</p>
</div>
<img class="img-responsive" src="{{ 'assets/images/nieuws/decathlon.png' | relative_url }}" alt="decathlon">

        

