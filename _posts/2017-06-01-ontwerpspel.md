---
layout: post
title: Het spel begint vorm te krijgen!
categories: post0
---
<div class="">
<h2 class="titel5">Het spel begint vorm te krijgen!</h2>
<p>Joepie! We zijn net begonnen aan het ontwerpen van het spel, en oh wat hebben we er zin in! Dit is de laatste stap in het ontwikkelen van GymCity, en daarna kan iedereen genieten van sporten met een doel. Hieronder vind je al enkele sneak peeks:</p>
</div>

<div class="item row" id="post">
    <div id="carouselExampleIndicators" class=" carousel slide col-sm-12 col-md-10 offset-md-1" data-ride="carousel">
        <div class="carousel-inner clearfix" role="listbox">
            <div class="carousel-item active">
            <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="{{ 'assets/images/nieuws/spel1.png' | relative_url }}" alt="schetsen van het spel">
            </div>
            </div>
            </div>
            <div class="carousel-item">
               <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="{{ 'assets/images/nieuws/spel2.png' | relative_url }}" alt="schetsen van het spel">
            </div>
            </div>
            </div>
            <div class="carousel-item">
             <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive foto-app" src="{{ 'assets/images/stappen/stap4.png' | relative_url }}" alt="de app">
            </div>
            </div>
            </div>
        </div>
        
    </div>
</div>

          

