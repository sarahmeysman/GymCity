---
layout: post
title: De 1e versie is klaar om te testen!
categories: post1
---

<h2 class="titel1">De 1e versie: klaar om te testen</h2>

<p>De eerste betaversie van onze app is uit! Klaar om getest te worden door jullie. Wij zijn ook klaar om al jullie input te ontvangen! Onze sponsors kregen al info over hoe je de app kan downloaden. Wil jij ook mee testen? Stuur ons 
een donatie en beleef het hele proces bij jou thuis.</p>
<img class="img-responsive" src="{{ 'assets/images/nieuws/eersteversie.png' | relative_url }}" alt="eerste versie van de app">

